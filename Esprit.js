class Esprit {

	constructor(config) {
		self = this

		//API
		//Phrase pour camoufler
		this.mask = (config.mask != undefined) ? config.mask : 'Esprit, veux tu repondre:'
		//Phrase pour la reponse
		this.sentence = (config.sentence != undefined) ? config.sentence : 'La reponse de l\'Esprit: '
		//Verification des champs
		this.minLength = (config.minLength != undefined) ? config.minLength : 3
		//Phrase si les champs ne sont pas valides
		this.error = (config.error != undefined) ? config.error : 'Esprit: Continue, tu m\'interresses'
		//Touche a appuyer pour activer le mode secret
		this.secretkey = (config.secretkey != undefined) ? config.secretkey : '.'
		//Si false, alors pas de loader, sinon, on ajoute ce tag
		this.loaderTag = (config.loaderTag != undefined) ? config.loaderTag : '<img src="loader.gif" alt="loader">'
		//Loader time in ms
		this.loaderTime = (config.loaderTime != undefined) ? config.loaderTime : 1200



		//Variables utiles pour le script (a ne pas toucher !)
		this.secretMode = false //Vrai si on active le mode secret (par default, on enclanche avec '.')
		this.response = '' //Reponse stockee ici


		//Variables du DOM
		this.DOMPetition = document.querySelector(config.idPetition) //PetitionInput
		this.DOMQuestion = document.querySelector(config.idQuestion) //QuestionInput
		this.DOMAnswer = document.querySelector(config.idAnswer) //DivAnswer
		this.DOMReset = document.querySelector(config.idReset) //ResetInput
		this.DOMSubmit = document.querySelector(config.idSubmit) //SubmitInput
	}

	enable() {
		this.DOMPetition.addEventListener('keydown', this._petitionKeydown)
		this.DOMPetition.addEventListener('keyup', this._petitionKeyup)
		this.DOMQuestion.addEventListener('keyup', this._questionKeyup)
		this.DOMReset.addEventListener('click', this._resetClick)
		this.DOMSubmit.addEventListener('click', this._submitClick)
	}

	_petitionKeydown(e) {
		//Si c'est une lettre/chiffre ('a', 'b', 5 ... etc)
		if (e.key.length == 1) {

			//Si pas de characteres dans le champs, on reset
			if (this.value.length == 0) {
				//Soit on fait ce code, soit on passe par une fonction generique (reset())
				/*
				self.secretMode = false
				self.response = ''
				*/
				self.reset()
			}

			//Si le formulaire est vide et que l'on appuie sur '.'
			if (this.value.length == 0 && e.key == self.secretkey) {
				//On n'affiche pas la lettre tapee
				e.preventDefault()

				//On ajoute la premiere lettre du mask dans le formulaire (input)
				this.value += self.mask[0]
				//On passe le mode secret a vrai
				self.secretMode = true
				//On arrete le script
				return true
			}

			//Si le mode secret est active et que l'on tape une lettre de la reponse
			if (this.value.length > 0 && self.secretMode) {
				e.preventDefault()

				//On ajoute les lettres a la phrase si on peut
				self.mask[this.value.length] != undefined ? this.value += self.mask[this.value.length] : false

				//Si on appuie une deuxieme fois sur le '.', alors on enleve le mode secret
				if (e.key == self.secretkey) {
					self.secretMode = false
					return false
				}

				//On sauvegarde dans la reponse ce que l'utilisateur tape
				self.response += e.key

				return true
			}
		}
	}

	_petitionKeyup(e) {
		//Si on ecrit ':', alors on passe au champs suivant
		if (e.key == ':') {
			self.DOMQuestion.focus()
		}


		//Si on appuie sur la touche
		if (e.key == 'Backspace') {
			//Si on supprime la response, alors on enleve les lettres dans la variable
			//Ou si on est en en secret mais on supprimer du texte
			if (self.secretMode || !self.secretMode && self.response.length >= this.value.length) {
				self.response = self.response.slice(0, -1);
			}

		}
	}

	_questionKeyup(e) {
		//Si on a fini la question et que l'on tape '?'
		if (e.key == '?') {
			self._verifyInputsAndShow()
		}
	}

	//Verifie les entrees et execute la fonction _showAnswer
	_verifyInputsAndShow() {
		//On teste si la question et petition sont bonnes ET qu'il y a une reponse
		if (self.DOMPetition.value.length > self.minLength && self.DOMQuestion.value.length > self.minLength && self.response.length > 0){
			self._showAnswer(true)
		} else {
			self._showAnswer(false)
		}
	}

	//Rendre la reponse
	_showAnswer(good) {
		let render = self.sentence

		//Affiche la reponse ou l'erreur selon le cas (+ rendu (ligne du dessous))
		//good ? render += self.response : render += self.error
		render += good ? self.response : self.error


		if (self.loaderTag == false){
			//Si pas de loader, on affiche direct
			self.DOMAnswer.innerHTML = render;

		} else {
			//Sinon, on fait l'annimation :
			self.DOMAnswer.innerHTML = '<img src="loader.gif" alt="loader">';
			setTimeout(function () {
				//Et on affiche finalement
				self.DOMAnswer.innerHTML = render;
		}, self.loaderTime) //Temps du loader
		}

	}

	//Reset when click on the button
	_resetClick() {
		self.reset();
	}

	reset() {
		self.response = ''
		self.DOMQuestion.value = ''
		self.DOMPetition.value = ''
		self.DOMAnswer.innerHTML = ''
	}

	//Submit
	_submitClick() {
		self._verifyInputsAndShow()
	}
}
